<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Validator;


class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $input = $request->all();
        for ($i=0; $i < count($input['name']); ++$i) 
{       
            $products= new Question;        
            $products->name = $input['name'][$i];
            $products->type= $input['type'][$i];
            // $products->selling_price= $input['selling_price'][$i];
            $products->save();  
}
        //  return $request;
        // foreach($request->input('name','type') as $key => $value) {
        //     dd($value);
            // Question::create(   ['name'=>$value]);
        }
        // foreach($request->input('type') as $key => $value) {
        //     Question::create(   ['type'=>$value]);
        // }

    //     $rules = [];


    //     foreach($request->input('name') as $key => $value) {
    //         $rules["name.{$key}"] = 'required';
    //     }


    //     $validator = Validator::make($request->all(), $rules);


    //     if ($validator->passes()) {


    //         foreach($request->input('name') as $key => $value) {
    //             Question::create(   ['name'=>$value]);
    //         }
    //         foreach($request->input('type') as $key => $value) {
    //             Question::create(   ['type'=>$value]);
    //         }


    //   }
  

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //
    }
}
