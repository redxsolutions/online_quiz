
<!DOCTYPE html>

<html>
            <head>
                <!-- ... -->
                <script type="text/javascript" src="{{asset('/bower_components/jquery/dist/jquery.min.js')}}"></script>
                <script type="text/javascript" src="{{asset('/bower_components/moment/min/moment.min.js')}}"></script>
                <script type="text/javascript" src="{{asset('/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
                <script type="text/javascript" src="{{asset('/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
                 <link rel="stylesheet" href="{{asset('/bower_components/bootstrap/css/bootstrap.min.css')}}" />
                <link rel="stylesheet" href="{{asset('/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
             </head>

<body>

        

<div class="container">

        <form  method="POST" enctype="multipart/form-data" action="{{ route('exams.store') }}">
                 {{-- <div >
                  <label for="exampleFormControlInput1">Name</label>
                  <input name="name" type="name" class="form-control" id="exampleFormControlInput1" placeholder="name">
                </div> --}}
                <div class="container">
                        <div class="row">
                            <div class='col-sm-6'>

                                    <div >
                                            <label for="exampleFormControlInput1">Name</label>
                                            <input name="name" type="name" class="form-control" id="exampleFormControlInput1" placeholder="name">
                                          </div>

                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input name="starting_date" type='text' class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class='input-group date' id='datetimepicker2'>
                                            <input name="ending_date" type='text' class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div >
                                            <label for="exampleFormControlInput2">Duration</label>
                                           <input name="duration" type="text" class="form-control" id="exampleFormControlInput2" placeholder="Duration">
                                       </div>
                            </div>
                            
                         
                        </div>
                    </div>

                {{-- <input type="text" value="2012-05-15 21:05" id="datetimepicker"> --}}
                {{-- <input type="text" value="2012-05-15 21:05" id="datetimepicker">
            <div>
                   
                    <label for="exampleFormControlInput1">Starting time</label>
                  <input name="starting_date" class="date form-control" type="text">
                </div>  
                <div>
                <label for="exampleFormControlInput1">Ending time</label>
                 <input name="ending_date" class="date form-control" type="text">
                </div>     
                        <div >
                             <label for="exampleFormControlInput2">Duration</label>
                            <input name="duration" type="text" class="form-control" id="exampleFormControlInput2" placeholder="Duration">
                        </div> --}}
                <button type="submit" class="btn btn-primary mb-2">Confirm </button>

                <input type="hidden" name="_token" value="{{ Session::token() }}">
              </form>


   
    

</div>





</body>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker();
    });
</script>
</html>